//
//  InterfaceController.swift
//  watchtest WatchKit Extension
//
//  Created by WSR on 6/22/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class InterfaceController: WKInterfaceController {

    @IBOutlet weak var city: WKInterfaceLabel!
    @IBOutlet weak var img: WKInterfaceImage!
    @IBOutlet weak var grad: WKInterfaceLabel!
    @IBOutlet weak var next: WKInterfaceButton!
    
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
